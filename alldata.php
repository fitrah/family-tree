<?php 
	include "header.php";
	error_reporting(E_ERROR);
?>
<div class="container-fluid">
  <div class="row">
	<div class="col-sm-12 col-md-12 main">
	  <?php 
	  $page = (isset($_GET['page'])) ? $_GET['page']:1;
	  $perpage = (isset($_GET['perpage'])) ? $_GET['perpage']:25;
	  
	  if (isset($_GET['s'])){
		  $db->where ("url like '%$_GET[s]%' or title like '%$_GET[s]%'");		  
	  }
	  $db->pageLimit = $perpage;
	  $i = (isset($_GET['page']) && $_GET['page']<>1) ? $_GET['page']*$db->pageLimit + 1:1;
	  // set page limit to 2 results per page. 20 by default
	  $products = $db->arraybuilder()->get("indomaret");
	  
	  ?>
	<div class="row">
		<a id="dlink"  style="display:none;"></a>
		<h2 class="sub-header">Data Hasil Crawler (<?= $db->count;?>)</h2>
		<div class="col-xs-12 col-sm-6" >
			<a href="#" onclick="tableToExcel('testTable', 'Produk', 'data_ikea.xls')" value="Export to Excel">
				<img src="asset/export/xls.png" width="24px"> Download XLS</a>
		</div>
	</div>
		<table class="table table-striped" id="testTable">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>Nama Produk</th>
			  <th>Breadcrumb</th>
			  <th>Price</th>
			  <th style="min-width:500px">Description</th>
			  <th>Image</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php foreach ($products as $key=>$prod) { 
			
		  ?>
			<tr>
			  <td><?=$i;?></td>
			  <td><?=$prod['title'];?></td>
			  <td><?=$prod['breadcrumb'];?></td>
			  <td><?=$prod['price'];?></td>
			  <td><?=$prod['description'];?></td>
			  <td><?=$prod['image'];?></td>
			  
			</tr>
			<tr><td colspan="10">
		  <?php $i++; } 
		  
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = $db->totalPages;
		$LastPagem1 = $lastpage - 1;
		$targetpage = "index.php";
		$paginate = '';
		$stages = 1;
		if($lastpage > 1)
		{
			$paginate .= "<nav><ul class='pagination'>";
			// Previous
			if ($page > 1){
			$paginate.= "<li><a href='$targetpage?page=$prev' aria-label='Previous'>
						<span aria-hidden='true'>&laquo;</span></a></li>";
			}else{
			$paginate.= "<li class='disabled'><span aria-hidden='true'>&laquo;</span></li>"; }

			// Pages
			if ($lastpage < 7 + ($stages * 2)) // Not enough pages to breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<li class='active'><a href='#'>$counter <span class='sr-only'>(current)</span></a></li>";
					}else{
						$paginate.= "<li><a href='$targetpage?page=$counter'>$counter</a></li>";
					}
				}
			}
			elseif($lastpage > 5 + ($stages * 2)) // Enough pages to hide a few?
			{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))
			{
			for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
			{
			if ($counter == $page){
			$paginate.= "<li class='active'><a href='#'>$counter <span class='sr-only'>(current)</span></a></li>";
			}else{
			$paginate.= "<li><a href='$targetpage?page=$counter'>$counter</a></li>";}
			}
			$paginate.= "<li class='active'><a href='#'>...<span class='sr-only'>(current)</span></a></li>";
			$paginate.= "<li><a href='$targetpage?page=$LastPagem1'>$LastPagem1</a></li>";
			$paginate.= "<li><a href='$targetpage?page=$lastpage'>$lastpage</a></li>";
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
			$paginate.= "<li><a href='$targetpage?page=1'>1</a></li>";
			$paginate.= "<li><a href='$targetpage?page=2'>2</a></li>";
			$paginate.= "<li class='active'><a href='#'>...<span class='sr-only'>(current)</span></a></li>";
			for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
			{
			if ($counter == $page){
			$paginate.= "<li class='active'><a href='#'>$counter <span class='sr-only'>(current)</span></a></li>";
			}else{
			$paginate.= "<li><a href='$targetpage?page=$counter'>$counter</a></li>";}
			}
			$paginate.= "<li class='active'><a href='#'>...<span class='sr-only'>(current)</span></a></li>";
			$paginate.= "<li><a href='$targetpage?page=$LastPagem1'>$LastPagem1</a></li>";
			$paginate.= "<li><a href='$targetpage?page=$lastpage'>$lastpage</a></li>";
			}
			// End only hide early pages
			else
			{
			$paginate.= "<li><a href='$targetpage?page=1'>1</a></li>";
			$paginate.= "<li><a href='$targetpage?page=2'>2</a></li>";
			$paginate.= "<li class='active'><a href='#'>...<span class='sr-only'>(current)</span></a></li>";
			for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
			{
			if ($counter == $page){
			$paginate.= "<li class='active'><a href='#'>$counter <span class='sr-only'>(current)</span></a></li>";
			}else{
			$paginate.= "<li><a href='$targetpage?page=$counter'>$counter</a></li>";}
			}
			}
			}

			// Next
			if ($page < $counter - 1){
			$paginate.= "<li><a href='$targetpage?page=$next' aria-label='Previous'>
						<span aria-hidden='true'>&raquo;</span></a></li>";
			}else{
			$paginate.= "<li class='disabled'><span aria-hidden='true'>&raquo;</span></li>";
			}

			$paginate.= "</ul></nav>";

		}
		echo $db->totalPages.' Results';
		// pagination
		echo $paginate;
		  
		  ?></td></tr>
		  </tbody>
		</table>
	  </div>
	</div>
  </div>
</div>
<script>
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        }, format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML
        }
   document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
    }
})()
</script>
<?php 
	function pembulatan($number, $decimal) {
		$koma = explode(".",$number);
		if (strlen($koma['1']) > 1 && $koma['1'] > 30){
			$number = $koma['0'] + 1;
		}elseif (strlen($koma['1']) <= 1 && $koma['1'] > 3){
			$number = $koma['0'] + 1;
		}
		else{
			$number = $koma['0'];
		}
		return $number;
	}
	include "footer.php";
?>