<?php
require_once ('db/MysqliDb.php');

set_time_limit(0);
error_reporting(E_ERROR);
$url = 'https://www.klikindomaret.com/';
$start = time(true);

$data = getCurl($url);
//$data = getProduct('https://www.klikindomaret.com/product/laptop-bag');

$i=0;
foreach ($data->getElementsByTagName('div') as $link_all) {
	if ($link_all->getAttribute('id') == "categories-Retail") {
		foreach ($link_all->getElementsByTagName('a') as $link_a) {
			$urlCategory = "https://www.klikindomaret.com".$link_a->getAttribute('href');
			echo "<br> link category ".$urlCategory;
			getList($urlCategory);
		}
	}
}

function getList($urlCategory){
	$dataList = getCurl($urlCategory);
	foreach ($dataList->getElementsByTagName('div') as $link_product) {
		if (strpos($link_product->getAttribute('id'),"categoryFilterProduct") !== false) {
			foreach ($link_product->getElementsByTagName('a') as $link_p) {
				$urlProduct = "https://www.klikindomaret.com".$link_p->getAttribute('href');
				echo "<br> link product ".$urlProduct;
				$data_product = getProduct($urlProduct);
			}
		}
	}
}
function getProduct($urlProduct){
	echo "url ".$urlProduct;
	$db = new MysqliDb ('localhost', 'root', '', 'indomaret');
	$doc = getCurl($urlProduct);
	//$regex = '/(\s|\\\\[rntv]{1})/';
	$regex = '/\s+/';
	foreach ($doc->getElementsByTagName('span') as $links) {
		if ($links->getAttribute('class') == "normal price-value") {
					$price = '';
					$price = $links->nodeValue;
					echo "<br>". $price = trim(preg_replace($regex, ' ', $price));
		}
	}
	foreach ($doc->getElementsByTagName('img') as $links) {
		if ($links->getAttribute('id') == "zoom_03") {
					$image = '';
					$image = $links->getAttribute('src');
					echo "<br>". $image = trim(preg_replace($regex, ' ', $image));
		}
	}
	$breadcrumb = '';
	foreach ($doc->getElementsByTagName('div') as $links) {
		if ($links->getAttribute('class') == "ellipsis") {
					$title = '';
					$title = $links->nodeValue;
					echo "<br>".$title = trim(preg_replace($regex, ' ', $title));
		}
		if ($links->getAttribute('class') == "briefproduct") {
					$description = '';
					$description = $links->nodeValue;
					echo "<br>".$description = trim(preg_replace($regex, ' ', $description));
		}
		if ($links->getAttribute('class') == "breadcrumb") {
					
					$breadcrumb = $links->nodeValue;
					echo "<br> breadcrumb : ".$breadcrumb = trim(preg_replace($regex, ' ', $breadcrumb));
		}
	}
	
	$content = array('url'=>$urlProduct,'title'=>trim($title),
				'price'=>clean_int($price),'breadcrumb'=>trim($breadcrumb),
				'description'=>trim($description),'image'=>$image);
				saveData($content);
}

function processPage($url){
	$doc = getCurl($url);
	foreach ($doc->getElementsByTagName('input') as $link) {
		if ($link->getAttribute('id') == "hidd-TotalPage") {
			$total_page = $link->getAttribute('value');
		}
	}
	for ($p=2;$p<=$total_page;$p++){
		$href = $url."?PageSize=24&Page=".$p;
		$data_product = getProduct($href);
	}
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
   $string = str_replace('-', ' ', $string); // Replaces all spaces with hyphens.

   return $string;
}

function clean_int($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^0-9\-.]/', '', $string); // Removes special chars.
   $string = str_replace('-', ' ', $string); // Replaces all spaces with hyphens.
   $string = trim($string);
   return $string;
}

function saveData($data){
	$db = new MysqliDb ('localhost', 'root', '', 'indomaret');
	// check data
	
	$db->where("id", md5($data['url']));
	$data_old = $db->getOne ("indomaret");
	if (isset($data_old)){
		echo "<br> Ada data lama <br>";
		if ($data_old['title'] <> $data['title'] || $data_old['description'] <> $data['description'] || $data_old['price'] <> $data['price']){
			echo "<br> update data lama";
			$data['updated_at'] = $db->now();
		
			$db->where ('id', md5($data['url']));
			if ($db->update ('indomaret', $data))
				echo $db->count . ' <br> records were updated';
			else
				echo '<br> update failed: ' . $db->getLastError();
		}
	}else{
		$data['id'] = md5($data['url']);
		$data['created_at'] = $db->now();
		$data['updated_at'] = $db->now();
		//print_r($data);
		echo "<br>========</br>";
		$id = $db->insert('indomaret', $data);
		//var_dump($id);	 
		if ($id){
			echo '<br>product was created. Id=' . $id;
		}
		else {
			echo '<br>insert failed: ' . $db->getLastError();
		}
	}
}



function getCurl($url){
	$ch = curl_init();

// set url
	curl_setopt($ch, CURLOPT_URL, $url);

	//return the transfer as a string
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

	// $output contains the output string
	$data = curl_exec($ch);

	// close curl resource to free up system resources
	curl_close($ch);
	
	$html = new DOMDocument();
	$html->loadHTML($data);
	
	return $html;
}

function xmlCurl($url){
	$ch = curl_init();

// set url
	curl_setopt($ch, CURLOPT_URL, $url);

	//return the transfer as a string
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

	// $output contains the output string
	$data = curl_exec($ch);

	// close curl resource to free up system resources
	curl_close($ch);
	return $data;
}

$end = time(true);
$total_time = $end - $start;

echo "<br> total time = ". $total_time ."<br>";

?>