-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for family
CREATE DATABASE IF NOT EXISTS `family` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `family`;

-- Dumping structure for table family.masters
CREATE TABLE IF NOT EXISTS `masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `obit_date` datetime DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `mother_id` int(11) DEFAULT NULL,
  `couple_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table family.masters: ~27 rows (approximately)
/*!40000 ALTER TABLE `masters` DISABLE KEYS */;
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'Eyang Balung Tunggal', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(2, '-', NULL, 'female', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(3, 'Alimu', NULL, 'male', NULL, NULL, NULL, 1, 2, 4, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(4, '-', NULL, 'female', NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(5, 'Muhammad Hambali', NULL, 'male', NULL, NULL, NULL, 3, 4, 6, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(6, '-', NULL, 'female', NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(9, 'Emoy Armiya', NULL, 'male', NULL, NULL, NULL, 5, 6, 10, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(10, 'Salnidjah', NULL, 'female', NULL, NULL, NULL, 7, 8, 9, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(11, 'Rodiah Sungkawati', NULL, 'female', NULL, NULL, NULL, 9, 10, 12, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(12, 'Yoyo Waryo Suryadinata', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(13, 'Yayah Robiah Supriyatini', NULL, 'female', NULL, NULL, NULL, 9, 10, 14, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(14, 'Maman', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(15, 'Nunu Nuriah Suwandani (Alm)', NULL, 'female', NULL, NULL, NULL, 9, 10, 16, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(16, 'Kartowo (Alm)', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(17, 'Umbung /Maysaroh (Alm)', NULL, 'female', NULL, NULL, NULL, 9, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(18, 'Endah Hamidah', NULL, 'female', NULL, NULL, NULL, 9, 10, 19, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(19, 'Nachrawi AP', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(20, 'Nandang Irawan', NULL, 'male', NULL, NULL, NULL, 9, 10, 21, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(21, 'Nurseha', NULL, 'female', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(22, 'Daka Udin', NULL, 'male', NULL, NULL, NULL, 9, 10, 23, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(23, 'Euis', NULL, 'female', NULL, NULL, NULL, NULL, NULL, 22, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(24, 'Uum Umasyah', NULL, 'female', NULL, NULL, NULL, 9, 10, 25, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(25, 'Usman Sumantri', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(26, 'Ucu Jumalia Ani', NULL, 'female', NULL, NULL, NULL, 9, 10, 27, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(27, 'Jumami', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(28, 'Iis Ratna Jumila', NULL, 'female', NULL, NULL, NULL, 9, 10, 29, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(29, 'Syaiful Bachri (Alm)', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 28, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(30, 'Dini Wardiani', NULL, 'female', NULL, NULL, NULL, 12, 11, 31, NULL, NULL, NULL, NULL);
INSERT INTO `masters` (`id`, `name`, `alias`, `gender`, `birth_date`, `obit_date`, `image`, `father_id`, `mother_id`, `couple_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(31, 'AM Kahfie', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 30, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `masters` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
